{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running, Visualizing, and Analyzing Molecular Dynamics (MD) Simulations ##\n",
    "In this notebook, you will use Python tools to play around with Molecular Dynamics (MD) simulations. In this example, we will prepare a system of interacting hexamers that may spontaneously self-assemble into hexameric sheets. The hexamers look like the following:\n",
    "\n",
    "<div>\n",
    "<img src=\"toy_hex.png\" width=\"250\"/>\n",
    "</div>\n",
    "   \n",
    "where the cyan spheres are the body of the molecule and the pink spheres are virtual interaction sites that will allow each hexamer to bind to other hexamers. This interaction is attractive and has the following Gaussian functional form:\n",
    "\n",
    "$$ U(r_{ij}) = -A \\exp{(-B r_{ij})} $$\n",
    "\n",
    "where $U$ is the potential energy in kcal/mol, $r_{ij}$ is the pair distance between sites in Angstroms, and $A$/$B$ are model parameters. \n",
    "\n",
    "All of our simulations will be performed using LAMMPS, an open-source MD package developed at Sandia National Lab."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What am I looking at right now?\n",
    "This is a \"Jupyter notebook\", a tool for using Python interactively. For example, you can quickly work with data, run simulations, visualize data, etc. then decide you want to see how your results might change if you change a parameter earlier in your workflow. If you've used Mathematica before, the idea behind a Jupyter notebook is very similar. You hit `shift-enter` or `shift-return` to execute the code in a \"cell\". \n",
    "\n",
    "**Beware:**\n",
    "- the good thing about notebooks is that they let you interact with your data in very flexible ways\n",
    "- the bad thing is that you can execute cells out of order and overwrite variables in ways you forget or didn't expect\n",
    "\n",
    "If you're getting weird results, it's best to either do `Cell->Run All` at the top to reset the entire notebook, or if really needed, `Kernel->Restart`\n",
    "\n",
    "Let us run our first cell (right below) by hitting `shift-enter` inside the cell. This cell will import Python modules that we will need for the rest of the exercise. Note that the left of the cell will have a \"star\" marker (i.e., \"In \\[*\\]\") when it is running and an integer (i.e., \"In \\[1\\]\")when it is done."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Time to setup the notebook! This might take a few minutes to finish.\n",
    "# We are importing modules/libraries that other folks have created\n",
    "# These libraries contain functions that will use (so that we don't reinvent the wheel)\n",
    "\n",
    "import numpy as np\n",
    "import mdtraj as md\n",
    "import nglview as ngl\n",
    "from lammps import IPyLammps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preparing the LAMMPS simulation ###\n",
    "In your folder, you have the following files: \n",
    "\n",
    "(1) **supercell.pdb** - this is a file format known as the 'Protein Data Bank' format and contains the initial coordinates of our system as well as topology information (names and indices of atoms, residues, etc.)\n",
    "\n",
    "(2) **system.data** - this is an input file used by LAMMPS and contains the coordinates and topology information for a single hexamer, which we will later tile to form the complete system\n",
    "\n",
    "(3) **input.setup** - this is an input file used by LAMMPS that determines most of the simulation parameters (force field definition, integration method, timestep, etc.)\n",
    "\n",
    "All of these files are text files so you should feel free to view their contents using your favorite text editor.\n",
    "\n",
    "In the next cell, we will prepare the LAMMPS simulation using our input files. We can further modify the simulation using the `lammps` module, as you will see."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L = IPyLammps() # this prepares the lammps Python object\n",
    "L.variable(\"GAUSSA equal 2.65\") # this sets the \"A\" parameter in the Gaussian to 2.65 kcal/mol\n",
    "L.file(\"input.setup\") # this loads in the other simulation settings\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Running the simulation ###\n",
    "\n",
    "Now that the simulation is prepared, we can run the simulation. Let us run a short simulation of 25000 steps to see what happens. You'll see that a file called `md.dcd` will be created.\n",
    "\n",
    "NOTE: This next cell make take a few minutes to run. You will know it is still busy if you see the star in \"In \\[*\\]\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L.run(25000) #run for 25000 steps, should take a few seconds to run"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualizing the trajectory ###\n",
    "\n",
    "You'll see in the output of the previous block that a bunch of text was printed out. This is the \"log\" of the simulation and contains useful information. For our purposes, we can skip this and instead visualize the actual trajectory.\n",
    "\n",
    "We will use `nglview` to display the trajectory. Once it loads, you can click play to see the atoms in motion!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "No such comm: 2a7f231a680b46ca95f64455971465d3\n",
      "No such comm: 2a7f231a680b46ca95f64455971465d3\n",
      "No such comm: 2a7f231a680b46ca95f64455971465d3\n",
      "No such comm: 2a7f231a680b46ca95f64455971465d3\n",
      "No such comm: 2a7f231a680b46ca95f64455971465d3\n"
     ]
    }
   ],
   "source": [
    "traj = md.load(\"md.dcd\", top=\"supercell.pdb\")\n",
    "view = ngl.show_mdtraj(traj)\n",
    "view.clear_representations()\n",
    "\n",
    "# show all sites as either cyan or pink\n",
    "view.add_spacefill(selection=\".1 or .2\", color=\"cyan\", radius=3.0)\n",
    "view.add_spacefill(selection=\".3\", color=\"pink\", radius=3.0)\n",
    "\n",
    "# add to output\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Extending the simulation ###\n",
    "\n",
    "Let us extend the simulations now. Using the current parameters, we should expect to see self-assembly after about 1,000,000 (1M or 1e6) steps. \n",
    "\n",
    "NOTE: This next cell make take a few minutes to run. You will know it is still busy if you see the star in \"In \\[*\\]\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L.run(1000000) #run for 1M steps, should take a few minutes to run (80 sec on my machine)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "traj = md.load(\"md.dcd\", top=\"supercell.pdb\")\n",
    "view = ngl.show_mdtraj(traj)\n",
    "view.clear_representations()\n",
    "\n",
    "# show all sites as either cyan or pink\n",
    "view.add_spacefill(selection=\".1 or .2\", color=\"cyan\", radius=3.0)\n",
    "view.add_spacefill(selection=\".3\", color=\"pink\", radius=3.0)\n",
    "\n",
    "# add to output\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Observation debrief ###\n",
    "\n",
    "What did you observe when you played those movies? Did you see aggregation of the hexamers into a cluster? What shape is the cluster?\n",
    "\n",
    "Keep this behavior in mind because now we are going to adjust the model parameters to see what happens. You should change the $A$ parameter to be lower (e.g., 2.0 kcal/mol) or higher (e.g., 4.0 kcal/mol). To do so, go back to the top of the notebook and change the line that originally says:\n",
    "\n",
    "`L.variable(\"GAUSSA equal 2.65\")`\n",
    "\n",
    "Note that the original simulation uses 2.65 kcal/mol. What do you think will happen in either scenario? Do you notice any changes in assembly behavior? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
