# SUMMET - Introduction to Programming and Computational Molecular Sciences
This repository contains Jupyter notebook exercies to introduce students to
Python and Molecular Dynamics Simulations.

To launch an interactive session (via Binder): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ajpak%2Fsummet-python/HEAD)

